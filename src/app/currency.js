import request from 'request';
import async from 'async';
import chalk from 'chalk';
// Modules
import currency from '../controllers/coins';
import daily from '../controllers/daily';


const log = console.log;
const BTC_MARKET_URL = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/';
const CURRENCY_RATE_URL = 'https://floatrates.com/daily/usd.json';

const getBtcMarketClap = () => {
  request(BTC_MARKET_URL, { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    const { id, symbol, price_usd } = body[0];
    // Save coin
    currency.saveCoins(id, symbol, price_usd).then(() => {
      log(chalk.yellow(`${symbol} coinmarketcap updated`));
    }, err => log(err));
  });
}

const getDailyUsd = () => {
  request(CURRENCY_RATE_URL, { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    async.forEachOf(body, (value, key, cb) => {
      const { code, name, rate, numericCode } = value;
      // Save rates
      daily.saveDaily(code, name, rate, numericCode).then(() => {
      }, err => cb(err));
      cb();
    }, (err) => {
      if (err) log(err);
      log(chalk.yellow('Daily currency rates updated.'));
    });
  });
}

export {
  getBtcMarketClap,
  getDailyUsd,
}

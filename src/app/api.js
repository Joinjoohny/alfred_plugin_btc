import numeral from 'numeral';
import { getCoins } from '../controllers/coins';
import { getDaily } from '../controllers/daily';

const getItemsCurrencies = amount => {
  const items = [];

  return new Promise((resolve, reject) => {
    getCoins().then(value => {
      // btc
      const { price_usd } = value[0];
      const price = numeral((price_usd * amount)).format('0,0');
      items.push({
        code: 'USD',
        price,
      });
      // Get daily, price for other currency
      getDaily().then(daily => {
        daily.forEach((item) => {
          const { code, rate } = item;
          items.push({
            code,
            price: numeral(((price_usd * amount) * rate)).format('0,0'),
          })
        });
        resolve(items);
      });
    }, err => reject(err));
  });
};

export {
  getItemsCurrencies,
}

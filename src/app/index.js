// Lib
const chalk = require('chalk');
const CronJob = require('cron').CronJob;
// Modules
const currencyWorker = require('./currency');

const log = console.log;

const initWorker = () => {
  log(chalk.yellow('Init worker started NOW.'));
  currencyWorker.getBtcMarketClap();
  currencyWorker.getDailyUsd();
  // Run jobs
  // Run cron job every 5 min
  new CronJob('0 */5 * * * *', () => {
    log(chalk.yellow('Currency worker Cron Job started NOW.'));
    currencyWorker.getBtcMarketClap();
    currencyWorker.getDailyUsd();
  }, null, true, 'Europe/Kiev');
};

module.exports = initWorker;

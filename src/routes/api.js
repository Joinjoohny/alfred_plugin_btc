import Joi from 'joi'
import express from 'express'

import { validate } from '../middlewares'
import * as currencyCtrl from '../controllers/currency'

const router = express.Router()

const currencySchema = Joi.object().keys({
  amount: Joi.number().min(0).required(),
})

router
  .get('/btc', validate(currencySchema, 'query'), currencyCtrl.currency)

export default router

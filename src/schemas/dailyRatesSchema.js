/**
 * Schema Definitions
 *
 */
const mongoose = require('mongoose');

const dailyRatesSchema = new mongoose.Schema({
  code: String,
  name: String,
  rate: Number,
  numericCode: String,
}, { timestamps: true });

module.exports = mongoose.model('dailyRates', dailyRatesSchema);

/**
 * Schema Definitions
 *
 */
const mongoose = require('mongoose');

const coinsSchema = new mongoose.Schema({
  id: String,
  symbol: String,
  price_usd: String,
}, { timestamps: true });

module.exports = mongoose.model('coins', coinsSchema);

const dailyRates = require('../schemas/dailyRatesSchema');

module.exports = {
  saveDaily: (code, name, rate, numericCode) => (dailyRates.findOneAndUpdate(
    { code },
    {
      $set: {
        name,
        rate,
        numericCode,
      },
    },
    { upsert: true, new: true })),
  getDaily: () => (dailyRates.find({})),
};

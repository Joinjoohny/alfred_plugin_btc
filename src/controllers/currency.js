import { error } from 'console'
import _ from 'lodash'

import * as demoHlpr from '../app/api'

const errorHandler = (res, e) => {
  error(e)
  return res.status(500).json(JSON.parse(_.get(e, 'message', {})))
}

const ctrlWrapper = callback => (req, res) =>
  new Promise((resolve, reject) =>
    callback(req)
      .then(response => resolve(res.status(200).json(response)))
      .catch(err => reject(errorHandler(res, err))),
  )

const currency = ctrlWrapper(({ query: { amount } }) => demoHlpr.getItemsCurrencies(amount))

export {
  currency,
}

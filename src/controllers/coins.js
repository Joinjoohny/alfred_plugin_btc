const Coins = require('../schemas/coinsSchema');

module.exports = {
  saveCoins: (id, symbol, price_usd) => (Coins.findOneAndUpdate(
    { id },
    {
      $set: {
        symbol,
        price_usd,
      },
    },
    { upsert: true, new: true })),

  getCoins: () => (Coins.find({})),
};

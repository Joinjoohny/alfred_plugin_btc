import 'dotenv/config'

import './config/mongo'
import './config/express'

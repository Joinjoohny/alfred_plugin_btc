import mongoose from 'mongoose'
import { log, error } from 'console'
import chalk from 'chalk';

mongoose.Promise = global.Promise;
const url = 'mongodb://127.0.0.1/howmuchbtc';
// Create mongo database connection
mongoose.connect(url, { useMongoClient: true })

mongoose.connection.on('connected', () => log(chalk.green('[MongoDB] is connected')))
mongoose.connection.on('disconnected', () => error('[MongoDB] is disconnected'))

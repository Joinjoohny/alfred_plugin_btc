import cors from 'cors'
import express from 'express'
import morgan from 'morgan'
import helmet from 'helmet'
import bodyParser from 'body-parser'
import { log } from 'console'
import chalk from 'chalk';
import path from 'path';

import { main, api } from '../routes'
import initWorker from '../app'

const { SERVER_PORT, SERVER_HOST, NODE_ENV } = process.env

initWorker();
const app = express()
app
  .use(morgan('dev')) // :method :url :status :response-time ms - :res[content-length]
  .use(bodyParser.json()) // Parse application/json
  .use(bodyParser.urlencoded({ extended: true })) // Parse application/x-www-form-urlencoded
  .use(cors()) // Enable Cross Origin Resource Sharing
  .use(helmet()) // Secure your app by setting various HTTP headers


app.set('view engine', 'html');

app.use(express.static(path.join(__dirname, '../../site/dist')));
app.get('/', (req, res) => res.render('index.html'));

app
  .disable('x-powered-by') // Disable 'X-Powered-By' header in response
  .disable('etag') // Remove No Cache Control

app
  .use('/api', main) // Main routes
  .use('/api/v1', api) // Api routes

app.listen(SERVER_PORT, SERVER_HOST, () => {
  if (NODE_ENV !== 'test')
    log(chalk.green('[Express] Api is running on port'), SERVER_PORT)
})

export default app
